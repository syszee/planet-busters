![pb_still-1024x576.png](https://bitbucket.org/repo/xznzGG/images/1469827424-pb_still-1024x576.png)

www.orangepixel.net

### what is this ###

Planet Busters started as an entry to the LibGDX jam, but I simply didn't have the time to continue working on it within the jam time! my bad..

I figured it would be interesting enough for some people to grab some ideas from the source or the game design, so the source code is available to toy around with or maybe even learn from.

I'm now using this project as test-case to improve the cleanness of my code, I'm not a purist of how code should be written, and I think people telling other people how code should be written are stupid and silly!  but since I'm a one-man company my code often is just quick, dirty, and understandable for me only and can use some improvements.

A lot of people will tell you how to write OOP stuff in your game code, and how to do this or that.. and most of them haven't completed a single game.. they often do not understand that a bunch of that stuff is simply not practical for various reasons.

So as I improve my "practical clean code" in my normal games, I also update this project with improvements I find handy yet usable in actual completed games. 

### what can we do with it ###

Learn from it, laugh at it, ignore it, but mostly if you are a beginning game developer: modify the shit out of this code.. that's the best way to learn programming.  Play the game, find something you think would be cool to change, and hunt down the part of the code that does that!  

Don't worry about how something "should" be done, the key to creating games is writing code that does what you want it to do. Improving, simplifying, enhancing, or optimizing all comes later.

### HOW!? ###

Download a copy of Eclipse (free), import this project (Java), and figure it out :)

I don't have time to teach people stuff, or help people out.. I'm sure you can figure this out on your own, that's part of the fun!