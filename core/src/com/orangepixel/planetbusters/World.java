package com.orangepixel.planetbusters;

import com.orangepixel.planetbusters.ai.Monsters;
import com.orangepixel.planetbusters.ai.m_Pickup;
import com.orangepixel.planetbusters.ai.m_Scenery;
import com.orangepixel.planetbusters.ai.m_Walkers;
/**
 * World contains the tilemap and all info which is part of the game world
 * level generator is a utility method of this class
 *  
 */
import com.orangepixel.utils.Light;

public class World {
	
	public final static int	tileMapW = 40;
	public final static int	tileMapH = 40;

	public final static int	tEMPTY = 0,
							tSOLID = 1,
							tMONSTER=2,
							tMARKER=3;

	public int worldOffsetX;
	public int worldOffsetY;
	public int difficulty;
	public boolean levelCompleted;
		
	int worldFloorY;
	
	int worldShake;
	int worldShakeOffsetX;
	int worldShakeOffsetY;
	
	int			Spriteset;
	
	int			lockScreen;
	boolean		lockVertical;
	boolean		CameraTakeOver;	// if something else besides player takes focus
	boolean		CameraIsView;	// signal player we are back on camera-position checking
	int			CameraTakeOverCountdown;
	int			lockVerticalValue;
	int			softLock;
	boolean		autoScroll;
	int			cameraTargetX;
	int			cameraTargetY;	
	
	
	
	
	
	int[] 		tileMap = new int[tileMapW*tileMapH];
	int[]		renderMap = new int[tileMapW*tileMapH];
	
	int			worldPre;
	int			worldPost;
	int			world;	// galaxy, increases with every X missions completed
	int			level;
	
	/* ===============
	 * initialise the world object
	 * cleans tilemap
	 * ===============
	 */
	public final void initWorld() {
		for (int x=0; x<tileMapW; x++) 
		{
			for (int y=0; y<tileMapH; y++) 
			{
				put(x,y,0);
			}
		}
		
		levelCompleted=false;
	}
	
	
	/* ===============
	 * initialise the world for a new game
	 * ===============
	 */
	public final void initNewGame() {
		CameraTakeOver=false;
		
		world=1;
		createGalaxy();
		
		worldFloorY=Render.height-22;
	}
	
	/* ===============
	 * Generate a "galaxy" filling the list with random names for various planets/missions
	 * ===============
	 */
	public final void createGalaxy() {
		worldPre=Globals.getRandom(Globals.galaxyNamePre.length);
		worldPost=Globals.getRandom(Globals.galaxyNamePost.length);

		for (int i=0; i<Mission.missionList.length; i++) 
		{
			Mission.set(i, 1+Globals.getRandom(3), world+(i+1), false, (world*5)+(Globals.getRandom(10)*5));
		}
	}
	
	/* ===============
	 * Clear the specified type from the tilemap (eg: markers)
	 * ===============
	 */
	public final void clearType(int myTile) {
		for (int x=0; x<tileMapW; x++) 
		{
			for (int y=0; y<tileMapH; y++) 
			{
				if (tileMap[x+(y*tileMapW)]==myTile) tileMap[x+(y*tileMapW)]=tEMPTY; 
			}
		}
	}
	
	/* ===============
	 * get specified tile
	 * ===============
	 */
	public final int getTile (int myX, int myY) {
		return tileMap[myX+(myY*tileMapW)];
	}
	
	/* ===============
	 * put specified tile at coords
	 * ===============
	 */
	public final void put (int myX, int myY, int myTile) {
		tileMap[myX+(myY*tileMapW)]=myTile;
	}
	
	/* ===============
	 * put specified tile in whole area
	 * ===============
	 */
	public final void putArea(int myX, int myY, int myW, int myH, int myTile) {
		for (int x=myX; x<myX+myW; x++) 
		{
			for (int y=myY; y<myY+myH; y++) 
			{
				put(x,y,myTile);
			}
		}
	}

	/* ===============
	 * rendermap contains the "Textures" used for every tile in the map
	 * this fetches the specified render tile
	 * ===============
	 */
	public final int getRendermap(int myX, int myY) {
		return renderMap[myX+(myY*tileMapW)];
	}
	
	/* ===============
	 * put specified texture at coords
	 * ===============
	 */
	public final void putRendermap(int myX, int myY, int myTile) {
		renderMap[myX+(myY*tileMapW)]=myTile;
	}
	
	/* ===============
	 * fill area with specified texture
	 * ===============
	 */
	public final void putAreaRendermap(int myX, int myY, int myW, int myH, int myTile) {
		for (int x=myX; x<myX+myW; x++) 
		{
			for (int y=myY; y<myY+myH; y++) 
			{
				putRendermap(x,y,myTile);
			}
		}
	}	
	
	/* ===============
	 * check if specific tile is a "solid" tile (includes monsters)
	 * ===============
	 */
	public final boolean isSolid(int myX, int myY) {
		if (myY<0) return false;
		if (myX<0 || myX>tileMapW || myY>tileMapH) return true;
		if (tileMap[myX+(myY*tileMapW)]>=tSOLID) return true;
		
		return false;
	}
	
	/* ===============
	 * force a camera take over (eg: monster doing something special)
	 * for set duration
	 * ===============
	 */
	public final void setCameraTakeOver(int myx,int myy,int duration) {
		CameraTakeOver=true;
		CameraTakeOverCountdown=duration;
		cameraTargetX=myx;
		cameraTargetY=myy;
	}
	
	/* ===============
	 * quick snap camera to specified coords
	 * ===============
	 */
	public final void setCamera(int x, int y) {
		cameraTargetX=worldOffsetX=x;
		cameraTargetY=worldOffsetY=y;
	}
	
	/* ===============
	 * update camera, centering it on specified player
	 * called every tick
	 * ===============
	 */
	public final void handleCamera(Player tmpPlayer) {
		int tx;
		int ty;

		if (CameraTakeOver) 
		{
			int xSpeed=((cameraTargetX- (Render.width>>1)) - worldOffsetX) >> 3;
			int ySpeed=((cameraTargetY- (Render.height>>1)) - worldOffsetY) >> 3;
			
			worldOffsetX+=xSpeed;
			worldOffsetY+=ySpeed;
		} 
		else 
		{
				
			tx=tmpPlayer.x+8;
			ty=tmpPlayer.y+10;
			
			int xSpeed=((tx- (Render.width>>1)) - worldOffsetX) >> 3;
			int ySpeed=((ty- (Render.height>>1)) - worldOffsetY) >> 3;
			
			worldOffsetX+=xSpeed;
			worldOffsetY+=ySpeed;
		}
		
		// correct stuff
		if (worldOffsetX<0) worldOffsetX=0;
		if (worldOffsetX>(tileMapW*16)-Render.width) worldOffsetX=(tileMapW*16)-Render.width;

		// don't check vertically top, cause our spaceship can fly above the map
//		if (worldOffsetY<0) worldOffsetY=0;
		if (worldOffsetY>(tileMapH*16)-Render.height) worldOffsetY=(tileMapH*16)-Render.height;
	}	
	
	
	
	/* ===============
	 * update world every tick
	 * ===============
	 */
	public final void update() {
		if (worldShake>0) worldShake--;
		if (CameraTakeOverCountdown>0) CameraTakeOverCountdown--;
		else CameraTakeOver=false;

		worldShakeOffsetY=0;
		worldShakeOffsetX=0;
		if (worldShake>0) 
		{
			if (worldShake>24) 
			{
				worldShakeOffsetY=Globals.getRandom(8)-4;
				worldShakeOffsetX=Globals.getRandom(8)-4;
			} 
			else if (worldShake>12) 
			{
				worldShakeOffsetY=Globals.getRandom(4)-2;
				worldShakeOffsetX=Globals.getRandom(4)-2;
			} 
			else 
			{
				worldShakeOffsetY=Globals.getRandom(2)-1;
				worldShakeOffsetX=Globals.getRandom(2)-1;
			}
		}	
		
		worldOffsetX+=worldShakeOffsetX;
		worldOffsetY+=worldShakeOffsetY;
	
		// place lights based on tiles
		int tx;
		int ty;
		
		tx=-worldOffsetX;
		for (int x=0; x<World.tileMapW; x++) 
		{
			ty=-worldOffsetY;
			for (int y=0; y<World.tileMapH; y++) 
			{
				
				// only add lights that are near the visible window
				if (tx>=-128 && ty>=-128 && tx<worldOffsetX+Render.width+128 && ty<worldOffsetY+Render.height+128) 
				{
					
					switch(renderMap[x+(y*World.tileMapW)]) 
					{
						case 3:	// wall light
							switch (Spriteset) 
							{
								case 1:
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,12,Light.LightType_SphereTense,117,221,255, 255);
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,48,Light.LightType_Sphere,29,175,243,255);
								break;
								
								case 2:
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,12,Light.LightType_SphereTense,238,122,255, 255);
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,48,Light.LightType_Sphere,207,45,231,255);
								break;

								case 3:
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,12,Light.LightType_SphereTense,219,249,171, 255);
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,48,Light.LightType_Sphere,202,253,121,255);
								break;
							}
						break;
						
						case 12: // floor lights
							switch (Spriteset) 
							{
								case 1:
									Light.addLight((x<<4)+4-worldOffsetX,(y<<4)+12-worldOffsetY,6,Light.LightType_SphereTense,255,223,46, 255);
									Light.addLight((x<<4)+11-worldOffsetX,(y<<4)+9-worldOffsetY,6,Light.LightType_SphereTense,255,223,46, 255);
									Light.addLight((x<<4)+9-worldOffsetX,(y<<4)+15-worldOffsetY,6,Light.LightType_SphereTense,255,223,46, 255);

									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+8-worldOffsetY,32,Light.LightType_Sphere,255,223,46,255);
								break;
								
								case 2:
									Light.addLight((x<<4)+7-worldOffsetX,(y<<4)+12-worldOffsetY,12,Light.LightType_SphereTense,238,122,255, 255);
									Light.addLight((x<<4)+7-worldOffsetX,(y<<4)+12-worldOffsetY,48,Light.LightType_Sphere,207,45,231,255);
								break;
								
								case 3:
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,12,Light.LightType_SphereTense,219,249,171, 255);
									Light.addLight((x<<4)+8-worldOffsetX,(y<<4)+10-worldOffsetY,48,Light.LightType_Sphere,202,253,121,255);
								break;
							}
						break;
					}
					
				}
				ty+=16;
			}
			tx+=16;
		}		
	
	}

	
	// [ LEVEL GENERATOR ]----------------------------------------------------------------------------
	/* ===============
	 * generate a new world
	 * ===============
	 */
	public final void generateWorld() {
		Levelroom[]	myRooms = new Levelroom[96];

		int tx;
		int ty;
		int roomID=0;
		int nextY=0;
		int corridorHeight;
		int itterateCount;
		
		// create a list of rooms
		for (int i=0; i<myRooms.length; i++) myRooms[i]=new Levelroom();

		// add top-planet floor to world
		for (int x=0; x<World.tileMapW; x++) 
		{
			put(x, 1,World.tSOLID);
		}

		// make underworld part completely solid
		for (int x=0; x<World.tileMapW; x++) 
		{
			for (int y=3; y<World.tileMapH; y++) 
			{
				put(x, y, World.tSOLID);
			}
		}
		
		Spriteset=Mission.getSpriteset(level);
		difficulty=Mission.getDifficulty(level);
		
		
		// location of entry point to underworld
		int startX=14;
		int previousStartX=14;
		nextY=5;
		
		// create starting corridor (entry into the underworld)
		for (int y=0; y<=nextY; y++) 
		{
			put(startX, y, World.tEMPTY);
			putRendermap(startX, y, 0);
		}
		nextY++;
		
		// add a solid crate to fall on in first room (so you can't fall on enemies)
		Monsters.addMonster(Monsters.mSCENERY, startX<<4,(nextY+1)<<4, m_Scenery.METALCRATE, this);
		
		// start building the underworld
		while (nextY+4<World.tileMapH-4) 
		{
			
			if (roomID!=0) 
			{
				nextY+=2;
				itterateCount=24;
				while (Math.abs(startX-previousStartX)<4 && itterateCount>0) 
				{
					startX=myRooms[roomID-1].x+Globals.getRandom(myRooms[roomID-1].width);
					itterateCount--;
				}
				
				if (itterateCount==0) break;
				
				// create corridor (entry into the underworld)
				corridorHeight=1+Globals.getRandom(3);
				
				for (int y=0; y<corridorHeight; y++) 
				{
					put(startX, nextY+y, World.tEMPTY);
					putRendermap(startX, nextY+y, 0);
				}
				
				// put opening to corridor in floor texture of previous room
				putRendermap(startX, myRooms[roomID-1].y+1, 11);
				
			
				nextY+=corridorHeight;
				
				// fill our previously created room with some inhabitants and pickups
				populateRoom(myRooms[roomID-1]);
			}
			
			
			
			
			// create room
			myRooms[roomID].width=8+Globals.getRandom(8);
			myRooms[roomID].height=2;
			myRooms[roomID].x=startX- Globals.getRandom(myRooms[roomID].width-2);
			if (myRooms[roomID].x<1) myRooms[roomID].x=1;
			if (myRooms[roomID].x+myRooms[roomID].width>World.tileMapW-1) myRooms[roomID].x=World.tileMapW-myRooms[roomID].width-1; 
			myRooms[roomID].y=nextY;
			
			// cut out the room from the solid underworld
			putArea(myRooms[roomID].x,myRooms[roomID].y,myRooms[roomID].width,myRooms[roomID].height,World.tEMPTY);
			
			// texture the room
			putAreaRendermap(myRooms[roomID].x,myRooms[roomID].y,myRooms[roomID].width,1, 2);
			putAreaRendermap(myRooms[roomID].x,myRooms[roomID].y+1,myRooms[roomID].width,1, 10);
			putRendermap(myRooms[roomID].x,myRooms[roomID].y,1);
			putRendermap(myRooms[roomID].x,myRooms[roomID].y+1,9);
			
			

			
			// randomly add light-tiles to corridor + floors
			itterateCount=myRooms[roomID].width>>2;
			while (itterateCount>0) 
			{
				// add lights into the wall textures at random spots
				putRendermap(myRooms[roomID].x+1+Globals.getRandom(myRooms[roomID].width-2), myRooms[roomID].y, 3);
				
				// add lights on the floor at random spots
				if (Spriteset==2)
					putRendermap(myRooms[roomID].x+1+Globals.getRandom(myRooms[roomID].width-2), myRooms[roomID].y+1, 12);
				
				itterateCount--;
			}
			
			
			// randomly add "pilar" tiles (if nothing else was placed)
			itterateCount=myRooms[roomID].width>>2;
			while (itterateCount>0) 
			{
				tx=Globals.getRandom(myRooms[roomID].width-2);
				
				if (getRendermap(myRooms[roomID].x+1+tx, myRooms[roomID].y)==2
					|| getRendermap(myRooms[roomID].x+1+tx, myRooms[roomID].y)==10) 
				{
						putRendermap(myRooms[roomID].x+1+tx, myRooms[roomID].y, 5);
						putRendermap(myRooms[roomID].x+1+tx, myRooms[roomID].y+1, 13);
				}
				
				itterateCount--;
			}
						
			
			roomID++;		
			previousStartX=startX;
			
		}
		
		
		// add planet core to last room
		itterateCount=myRooms[roomID-1].width;
		tx=myRooms[roomID-1].x+1+Globals.getRandom(myRooms[roomID-1].width-2);
		while (itterateCount>0) 
		{
			ty=myRooms[roomID-1].y+1;
			if (!isSolid(tx, ty) && isSolid(tx, ty+1)) 
			{
				Monsters.addMonster(Monsters.mSCENERY, tx<<4,ty<<4, m_Scenery.PLANETCORE, this);
				itterateCount=0;
			} 
			else 
			{
				tx++;
				if (tx>myRooms[roomID].x+myRooms[roomID].width) tx=myRooms[roomID].x+1;
			}
			itterateCount--;
		}
		
		// clear markers
		clearType(World.tMARKER);
		setCamera(0, -256);
		

		
		// Add the player ship, triggering the player's drop
		Monsters.addMonster(Monsters.mSHIP, 48, (-3)<<4, 0, this);
		Monsters.addMonster(Monsters.mSCENERY, (14<<4)-8, 8, 0, this);
		

	}
	
	
	/* ===============
	 * populate the specified room with entities
	 * ===============
	 */
	public final void populateRoom(Levelroom myRoom) {
		int tx;
		int ty;
		
		// add metal crates randomly
		int itterateCount=myRoom.width>>3;
		while (itterateCount>0) 
		{
			tx=myRoom.x+1+Globals.getRandom(myRoom.width-2);
			ty=myRoom.y+1;
			if (!isSolid(tx, ty) && isSolid(tx, ty+1)) 
			{
				Monsters.addMonster(Monsters.mSCENERY, tx<<4,ty<<4, m_Scenery.METALCRATE, this);
			}
			itterateCount--;
		}
		
		// add health
		itterateCount=Globals.getRandom(myRoom.width>>2);
		while (itterateCount>0) 
		{
			tx=myRoom.x+1+Globals.getRandom(myRoom.width-2);
			ty=myRoom.y;
			if (!isSolid(tx, ty)) 
			{
				Monsters.addMonster(Monsters.mPICKUP, tx<<4,ty<<4, m_Pickup.HEARTH, this);
			}
			itterateCount--;
		}		
		
		
		
		// add some monsters
		itterateCount=myRoom.width>>2;
		int monsterType;
		
		int maxDifficulty=8*difficulty;
		
		while (itterateCount>0 && maxDifficulty>0) 
		{
			monsterType=Globals.getRandom(3);
			
			tx=myRoom.x+1+Globals.getRandom(myRoom.width-2);
			ty=myRoom.y+1;

			if (!isSolid(tx, ty) && isSolid(tx, ty+1)) 
			{
				switch (monsterType) 
				{
					case 0: 
						if (difficulty<3 || Globals.getRandom(100)>90) 
						{ 
							Monsters.addMonster(Monsters.mWALKERS, tx<<4,ty<<4, m_Walkers.WORM, this);
							maxDifficulty--;
						}
					break;
					
					case 1:
						if (difficulty>1 || Globals.getRandom(100)>90) 
						{
							Monsters.addMonster(Monsters.mWALKERS, tx<<4,ty<<4, m_Walkers.ROBOEYE, this);
							maxDifficulty-=2;
						}
					break;
					
					case 2:
						if (difficulty>1 || Globals.getRandom(100)>90) 
						{
							Monsters.addMonster(Monsters.mBOUNCEXPLODE, tx<<4,ty<<4, 0, this);
							maxDifficulty-=2;
						}
					break;
					
				}
			}
			itterateCount--;
		}			
	}
	
}
